@tool
@icon("./assets/BoxMesh.svg")
extends "res://usefulScripts/nodes/BasicShape.gd"
class_name BasicBox3D

func new_mesh() -> PrimitiveMesh:
	return BoxMesh.new()
	
func new_shape() -> Shape3D:
	return BoxShape3D.new()

@export var same_image_on_all_sides : bool = true:
	set(value):
		same_image_on_all_sides = value
		if is_inside_tree() and mesh_instance.material_override != null:
			if same_image_on_all_sides: 
				mesh_instance.material_override.uv1_scale = Vector3(3, 2, 1)
			else: 
				mesh_instance.material_override.uv1_scale = Vector3(1, 1, 1)
		notify_property_list_changed()

@export var update_material_scale : bool = true

var previous_material_override = null

func _process(_delta):
	super._process(_delta)
	if Engine.is_editor_hint() and update_material_scale:
		if mesh_instance.material_override != null and previous_material_override != mesh_instance.material_override.get_instance_id():
			previous_material_override = mesh_instance.material_override.get_instance_id()
			if same_image_on_all_sides:
				mesh_instance.material_override.uv1_scale = Vector3(3, 2, 1)
	pass


